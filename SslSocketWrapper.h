#pragma once

#include <utility>
#include <memory>
#include <stdexcept>
#include <string>
#include <cassert>
#include <iostream>

#include "mbedtls/ssl.h"

template<typename Socket>
class SslSocketWrapper {
  mbedtls_ssl_context sslContext;
  std::shared_ptr<Socket> socketPtr;
  bool isValid;
  bool hasHandshaken;

public:
  SslSocketWrapper() : isValid(false), hasHandshaken(false) {
    mbedtls_ssl_init(&sslContext);
  }

  ~SslSocketWrapper() {
    mbedtls_ssl_free(&sslContext);
  }

  void initialize(const mbedtls_ssl_config& config, std::shared_ptr<Socket> initializedSocket) {
    socketPtr = initializedSocket;
    if (isValid) {
      throw std::invalid_argument("SSL: Double initialization is not supported.");
    }

    // Apply the configuration to the ssl context
    int res = mbedtls_ssl_setup(&sslContext, &config);
    if (res != 0) {
      throw std::invalid_argument("mbedtls_ssl_setup failed with code " + std::to_string(res));
    }

    // Set the io callbacks
    mbedtls_ssl_set_bio(&sslContext, static_cast<void*>(this), sendEncryptedS, recvEncryptedS, nullptr);
    isValid = true;
  }

  int send(const void* data, size_t size) {
    assert(isValid);
    if (!hasHandshaken) {
      handshake();
    }

    int res = mbedtls_ssl_write(&sslContext, static_cast<const unsigned char *>(data), size);
    if (res < 0) {
      throw std::invalid_argument("mbedtls_ssl_write failed with code " + std::to_string(res));
    }

    return res;
  }

  int recv(void* data, size_t size) {
    assert(isValid);
    if (!hasHandshaken) {
      handshake();
    }

    int res = mbedtls_ssl_read(&sslContext, static_cast<unsigned char *>(data), size);
    if (res < 0) {
      throw std::invalid_argument("mbedtls_ssl_read failed with code " + std::to_string(res));
    }

    return res;
  }

  void handshake() {
    std::cerr << isValid << ", " << hasHandshaken << std::endl;
    assert(isValid);
    assert(!hasHandshaken);

    int res = mbedtls_ssl_handshake(&sslContext);
    if (res == 0) {
      hasHandshaken = true;
    } else {
      throw std::invalid_argument("mbedtls_ssl_handshake failed with code " + std::to_string(res));
    }
  }

  static int sendEncryptedS(void* wrapper_, const unsigned char* buffer, size_t size) {
    SslSocketWrapper* wrapper = static_cast<SslSocketWrapper*>(wrapper_);
    return wrapper->sendEncrypted(buffer, size);
  }

  static int recvEncryptedS(void* wrapper_, unsigned char* buffer, size_t size) {
    SslSocketWrapper* wrapper = static_cast<SslSocketWrapper*>(wrapper_);
    return wrapper->recvEncrypted(buffer, size);
  }

  int sendEncrypted(const unsigned char* buffer, size_t size) {
    std::cerr << "sendEncrypted" << std::endl;
    try {
      return socketPtr->send(buffer, size);
    } catch (std::exception& e) {
      return MBEDTLS_ERR_SSL_WANT_WRITE;
    }
  }

  int recvEncrypted(unsigned char* buffer, size_t size) {
    std::cerr << "recvEncrypted" << std::endl;
    try {
      return socketPtr->recv(buffer, size);
    } catch (std::exception& e) {
      return MBEDTLS_ERR_SSL_WANT_READ;
    }
  }

  int notifyClose() {
    return mbedtls_ssl_close_notify(&sslContext);
  }

  template<typename ... Args>
  static std::shared_ptr<SslSocketWrapper> createServerAndWait(const mbedtls_ssl_config& config, Args... args) {
    std::shared_ptr<Socket> socket = Socket::createServerAndWait(args...);
    std::shared_ptr<SslSocketWrapper> result = std::make_shared<SslSocketWrapper>();
    result->initialize(config, socket);
    result->handshake();
    return result;
  }

  template<typename ... Args>
  static std::shared_ptr<SslSocketWrapper> connectToServer(const mbedtls_ssl_config& config, Args... args) {
    std::shared_ptr<Socket> socket = Socket::connectToServer(args...);
    std::shared_ptr<SslSocketWrapper> result = std::make_shared<SslSocketWrapper>();
    result->initialize(config, socket);
    result->handshake();
    return result;
  }
};
