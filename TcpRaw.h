#pragma once

#include <cstdio>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <memory>
#include <string>
#include <iostream>

class TcpRaw {
  int socketFileDescriptor;

public:
  TcpRaw(int socketFileDescriptor) : socketFileDescriptor(socketFileDescriptor) {}
  ~TcpRaw() { close(socketFileDescriptor); }

  int send(const void* data, size_t size) {
    std::cerr << "tcp send " << size << std::endl;
    int code = ::send(socketFileDescriptor, data, size, 0);
    if (code < 0) {
      throw std::invalid_argument("send failed with code " + std::to_string(code));
    }

    return code;
  }

  int recv(void* data, size_t size) {
    std::cerr << "tcp recv " << size << std::endl;
    int code = ::recv(socketFileDescriptor, data, size, 0);
    if (code < 0) {
      throw std::invalid_argument("recv failed with code " + std::to_string(code));
    }

    return code;
  }

  static std::shared_ptr<TcpRaw> createServerAndWait(int port) {
    int fd, err;
    sockaddr_in server;

    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
      throw std::invalid_argument("Can not create socket");
    }

    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    server.sin_addr.s_addr = htonl(INADDR_ANY);

    int opt_val = 1;
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt_val, sizeof opt_val);

    err = bind(fd, reinterpret_cast<sockaddr*>(&server), sizeof(server));
    if (err < 0) {
      throw std::invalid_argument("Can not bind socket");
    }

    err = listen(fd, 128);
    if (err < 0) {
      throw std::invalid_argument("Can not listen socket");
    }

    struct sockaddr_in client;
    int client_fd = 0;
    socklen_t client_len = sizeof(client);
    client_fd = accept(fd, reinterpret_cast<sockaddr*>(&client), &client_len);

    if (client_fd < 0) {
      throw std::invalid_argument("Can not establish new connection");
    }

    close(fd);
    return std::make_shared<TcpRaw>(client_fd);
  }

  static std::shared_ptr<TcpRaw> connectToServer(std::string host, int port) {
    std::cerr << "Connect to server with host = " << host << " and port = " << port << std::endl;
    int sockfd;
    hostent *he;
    sockaddr_in their_addr;

    if ((he = gethostbyname(host.c_str())) == nullptr) {
      throw std::invalid_argument("Can not resolve host");
    }

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      throw std::invalid_argument("Can not create socket");
    }

    their_addr.sin_family = AF_INET;
    their_addr.sin_port = htons(port);
    their_addr.sin_addr = *((in_addr *)he->h_addr);
    bzero(&(their_addr.sin_zero), 8);

    if (connect(sockfd, (sockaddr *)&their_addr, sizeof(sockaddr)) == -1) {
      throw std::invalid_argument("Can not connect");
    }

    std::cerr << "Connected!" << std::endl;
    return std::make_shared<TcpRaw>(sockfd);
  }
};
