#include "UdpRaw.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>

UdpRaw::~UdpRaw() {
  if (fd != 0) {
    close(fd);
  }
}

UdpRaw UdpRaw::connectToServer(std::string host, int port) {
  const int BUFFER_SIZE = 10;

  sockaddr_in servaddr;
  int udpSocketDescriptor = socket(AF_INET, SOCK_DGRAM, 0);
  if (udpSocketDescriptor < 0){
    throw std::invalid_argument("Can not get host address");
  }

  bzero(&servaddr, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr(host.c_str());
  servaddr.sin_port = htons(port);

  std::string msg = "hello";
  int count = sendto(udpSocketDescriptor, msg.c_str(), msg.size() + 1, 0, reinterpret_cast<sockaddr*>(&servaddr), sizeof(servaddr));
  if (count < 0) {
    ::close(udpSocketDescriptor);
    throw std::invalid_argument("cannot send message");
  }

  sockaddr_in clientaddr;
  unsigned int clientAddressLen = sizeof(clientaddr);

  char buf[BUFFER_SIZE];
  int recieved = recvfrom(udpSocketDescriptor, buf, BUFFER_SIZE, 0, reinterpret_cast<sockaddr*>(&clientaddr), &clientAddressLen);

  if (recieved < 0) {
      throw std::invalid_argument("Can not recive data, stop server");
  }

  if (std::string(buf) != "hello") {
    std::cerr << "Recieved " << recieved << std::endl;
    throw std::invalid_argument(std::string("Invalid messages ") + std::string(buf));
  }

  // Now determine who send data
  hostent *clientHostInfo = gethostbyaddr(reinterpret_cast<const char *>(&clientaddr.sin_addr.s_addr), sizeof(clientaddr.sin_addr.s_addr), AF_INET);

  if (clientHostInfo == nullptr) {
      throw std::invalid_argument("Can not get info about connected client");
  }

  char* hostAddressP = inet_ntoa(clientaddr.sin_addr);
  if (hostAddressP == nullptr) {
      throw std::invalid_argument("Can not get host address");
  }

  return UdpRaw(udpSocketDescriptor, clientaddr);
}

UdpRaw UdpRaw::createServerAndWait(int port) {
  const int MAX_TIMEOUT_MCS = 10000000;
  const int BUFFER_SIZE = 10;

  int udpSocketFileDescriptor = socket(AF_INET, SOCK_DGRAM, 0);
  if (udpSocketFileDescriptor < 0) {
    throw std::invalid_argument("Can not create socket");
  }

  timeval tv;
  tv.tv_sec  = MAX_TIMEOUT_MCS / 1000000ull;
  tv.tv_usec = MAX_TIMEOUT_MCS % 1000000ull;
  if (setsockopt(udpSocketFileDescriptor, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
      throw std::invalid_argument("Can not set option to socket");
  }

  const socklen_t sockaddrSize = sizeof(sockaddr);
  sockaddr_in serverSocketAddress;
  bzero(&serverSocketAddress, sockaddrSize);
  serverSocketAddress.sin_family      = AF_INET;
  serverSocketAddress.sin_addr.s_addr = htonl(INADDR_ANY);
  serverSocketAddress.sin_port        = htons(port);

  if (bind(udpSocketFileDescriptor, reinterpret_cast<sockaddr*>(&serverSocketAddress), sockaddrSize) == -1) {
      throw std::invalid_argument("Can not bind socket");
  }

  sockaddr_in clientaddr;
  unsigned int clientAddressLen = sizeof(clientaddr);
  char buf[BUFFER_SIZE];

  long recieved = recvfrom(udpSocketFileDescriptor, buf, BUFFER_SIZE, 0, reinterpret_cast<sockaddr*>(&clientaddr), &clientAddressLen);

  if (recieved < 0) {
    throw std::invalid_argument("Can not recive data, stop server");
  }

  if (std::string(buf) != "hello") {
    throw std::invalid_argument(std::string("Invalid messages ") + std::string(buf));
  }

  UdpRaw res(udpSocketFileDescriptor, clientaddr);
  res.writeRaw(std::string(buf).size() + 1, reinterpret_cast<uint8_t*>(buf));
  return res;
}

long UdpRaw::writeRaw(std::size_t size, const uint8_t* data) {
  return sendto(fd, data, size, 0, reinterpret_cast<sockaddr*>(&addr), sizeof(addr));
}

long UdpRaw::readRaw(std::size_t size, uint8_t* data, sockaddr_in& clientaddr_) {
  sockaddr_in clientaddr;
  unsigned int clientAddressLen = sizeof(clientaddr);

  long recieved = recvfrom(fd, data, size, 0, reinterpret_cast<sockaddr*>(&clientaddr), &clientAddressLen);

  if (recieved < 0) {
    return recieved;
  }

  clientaddr_ = clientaddr;
  return recieved;
}

long UdpRaw::readRaw(std::size_t size, uint8_t* data) {
  sockaddr_in addr;
  return readRaw(size, data, addr);
}
