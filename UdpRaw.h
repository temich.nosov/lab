#pragma once

#include <string>
#include <cstdint>

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

class UdpRaw {
  sockaddr_in addr;
  int fd;

public:
  UdpRaw(int fd, sockaddr_in& addr) : addr(addr), fd(fd) {}
  ~UdpRaw();
  UdpRaw(const UdpRaw&) = delete;
  void operator=(const UdpRaw&) = delete;

  UdpRaw(UdpRaw&& o) {
    fd = o.fd;
    addr = o.addr;
    o.fd = 0;
  }

  static UdpRaw connectToServer(std::string host, int port);
  static UdpRaw createServerAndWait(int port);

  long writeRaw(std::size_t size, const uint8_t* data);
  long readRaw(std::size_t size, uint8_t* data, sockaddr_in& clientaddr_);
  long readRaw(std::size_t size, uint8_t* data);
};
