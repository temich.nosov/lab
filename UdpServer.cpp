#include "UdpServer.h"

std::vector<uint8_t> UdpServer::readRaw() {
  const int BUFFER_SIZE = 128;
  std::vector<uint8_t> data(BUFFER_SIZE);
  long res = udpRaw.readRaw(BUFFER_SIZE, data.data());
  if (res <= 0) {
    close();
    return std::vector<uint8_t>();
  }

  data.resize(res);
  return data;
}

std::shared_ptr<UdpServer> UdpServer::connectTo(std::string host, int port) {
  return std::make_shared<UdpServer>(UdpRaw::connectToServer(host, port));
}

std::shared_ptr<UdpServer> UdpServer::launchServerAndWait(int port) {
  return std::make_shared<UdpServer>(UdpRaw::createServerAndWait(port));
}

int UdpServer::read(uint8_t* data, std::size_t size) {
  if (isClosed() && readedCount == readedData.size()) {
    return -1;
  }

  // Ok, wait for read enought data
  bool ok = false;

  do {
    {
      std::lock_guard guard(readLock);
      if (isClosed()) {
        if (readedCount == readedData.size()) {
          return -1;
        }

        ok = true;
      } else {
        ok = (readedData.size() - readedCount) >= size;
      }
    }

    if (ok) {
      std::lock_guard guard(readLock);
      size = std::min(size, readedData.size() - readedCount);
      copy(readedData.begin() + readedCount, readedData.begin() + readedCount + size, data);
      readedCount += size;
      return size;
    } else {
      std::this_thread::sleep_for(std::chrono::microseconds(10000));
    }
  } while (!ok);
}

int UdpServer::write(uint8_t* data, std::size_t size) {
  return udpRaw.writeRaw(size, data);
}
