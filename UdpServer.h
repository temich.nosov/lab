#pragma once

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdexcept>
#include <iostream>
#include <vector>
#include <thread>
#include <memory>

#include "UdpRaw.h"

class UdpServer {
  UdpRaw udpRaw;

  std::thread readThread;
  std::mutex readLock;
  std::vector<uint8_t> readedData;
  std::size_t readedCount;

  std::atomic_bool run;

  std::vector<uint8_t> readRaw();

  void readThreadMethod() {
    while (run.load()) {
      auto data = readRaw();
      if (data.size()) {
        std::lock_guard guard(readLock);
        std::copy(data.begin(), data.end(), std::back_inserter(readedData));
      }
    }
  }

public:
  UdpServer(UdpRaw&& udpRaw)
    : udpRaw(std::move(udpRaw)), readedCount(0), run(true)
  {
    readThread = std::thread(&UdpServer::readThreadMethod, this);
  }

  static std::shared_ptr<UdpServer> connectTo(std::string host, int port);
  static std::shared_ptr<UdpServer> launchServerAndWait(int port);

  int read(uint8_t* data, std::size_t size);
  int write(uint8_t* data, std::size_t size);

  bool isClosed() { return !run.load(); }

  ~UdpServer() {
    run = false;
    readThread.join();
  }

  void close() {
    run.store(false);
  }
};
