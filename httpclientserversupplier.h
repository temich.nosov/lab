#pragma once

#include <algorithm>
#include <vector>

template<typename Socket>
class HttpClientServerSupplier {
public:
  template<typename Func, typename... Args>
  static void startServer(Func processor, Args... args) {
    while (true) {
      auto s = Socket::createServerAndWait(args...);
      std::vector<uint8_t> data;
      bool run = true;
      while (run) {
        std::vector<uint8_t> buffer(128);
        try {
          int dx = s->recv(buffer.data(), buffer.size());
          if (dx > 0) {
            buffer.resize(dx);
            std::copy(buffer.begin(), buffer.end(), std::back_inserter(data));
          } else {
            run = false;
          }
        } catch (std::exception& e) {
          run = false;
        }
      }

      std::vector<uint8_t> answerData = processor(data);
      size_t sended = 0;
      bool finish = false;
      while (sended < answerData.size() && !finish) {
        try {
          int delta = s->send(answerData.data(), answerData.size() - sended);
          sended += delta;
          if (delta < 0) {
            finish = true;
          }
        } catch (std::exception& e) {
          finish = false;
        }
      }
    }
  }

  template<typename Func, typename... Args>
  static void connectToServer(Func processor, std::vector<uint8_t> sendData, Args... args) {
    auto s = Socket::connectToServer(args...);

    size_t sended = 0;
    bool finish = false;
    while (sended < sendData.size() && !finish) {
      try {
        int delta = s->send(sendData.data(), sendData.size() - sended);
        sended += delta;
        if (delta < 0) {
          finish = true;
        }
      } catch (std::exception& e) {
        finish = false;
      }
    }

    std::vector<uint8_t> data;
    bool run = true;
    while (run) {
      std::vector<uint8_t> buffer(128);
      try {
        int dx = s->recv(buffer.data(), buffer.size());
        if (dx > 0) {
          buffer.resize(dx);
          std::copy(buffer.begin(), buffer.end(), std::back_inserter(data));
        } else {
          run = false;
        }
      } catch (std::exception& e) {
        run = false;
      }
    }

    processor(data);
  }
};
