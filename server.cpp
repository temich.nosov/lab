#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#include <stdlib.h>
#define mbedtls_time       time
#define mbedtls_time_t     time_t
#define mbedtls_fprintf    fprintf
#define mbedtls_printf     printf
#endif

#define DEBUG_LEVEL 4

#include "SslSocketWrapper.h"
#include "UdpServer.h"
#include "TcpRaw.h"

#include <string>

#include "mbedtls/net_sockets.h"
#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/error.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/certs.h"

#if DEBUG_LEVEL > 0
#include "mbedtls/debug.h"
#endif

using Socket = TcpRaw;
using SslConnection = SslSocketWrapper<Socket>;

void configInit(mbedtls_ssl_config& config) {
  mbedtls_ssl_config_init(&config);
  mbedtls_ssl_config_defaults(
    &config,
    MBEDTLS_SSL_IS_CLIENT,
    MBEDTLS_SSL_TRANSPORT_STREAM,
    MBEDTLS_SSL_PRESET_DEFAULT
  );
  mbedtls_ssl_conf_authmode(&config, MBEDTLS_SSL_VERIFY_NONE);
}

template<typename F>
void execWithConfigClient(F&& f) {
  const char *pers = "ssl_client1";

  mbedtls_entropy_context entropy;
  mbedtls_ssl_config conf;
  mbedtls_x509_crt cacert;
  mbedtls_ctr_drbg_context ctr_drbg;

  mbedtls_ssl_config_init(&conf);
  mbedtls_x509_crt_init(&cacert);
  mbedtls_entropy_init(&entropy);
  mbedtls_ctr_drbg_init(&ctr_drbg);

  {
    int ret = mbedtls_ctr_drbg_seed(
          &ctr_drbg,
          mbedtls_entropy_func,
          &entropy,
          (const unsigned char *) pers,
          strlen(pers));
    if (ret < 0) throw std::invalid_argument("mbedtls_ctr_drbg_seed");
  }

  {
    int ret = mbedtls_x509_crt_parse(
          &cacert,
          reinterpret_cast<const unsigned char*>(mbedtls_test_cas_pem),
          mbedtls_test_cas_pem_len
    );
    if (ret < 0) throw std::invalid_argument("mbedtls_x509_crt_parse");
  }

  {
    int ret = mbedtls_ssl_config_defaults(
          &conf,
          MBEDTLS_SSL_IS_CLIENT,
          MBEDTLS_SSL_TRANSPORT_STREAM,
          MBEDTLS_SSL_PRESET_DEFAULT
    );
    if (ret < 0) throw std::invalid_argument("mbedtls_ssl_config_defaults");
  }

  mbedtls_ssl_conf_rng(&conf, mbedtls_ctr_drbg_random, &ctr_drbg);
  mbedtls_ssl_conf_authmode(&conf, MBEDTLS_SSL_VERIFY_OPTIONAL);
  mbedtls_ssl_conf_ca_chain(&conf, &cacert, nullptr);

  f(conf);

  mbedtls_x509_crt_free(&cacert);
  mbedtls_ssl_config_free(&conf);
  mbedtls_entropy_free(&entropy);
  mbedtls_ctr_drbg_free(&ctr_drbg);
}

template<typename F>
void execWithConfigServer(F&& f) {
  const char *pers = "ssl_server1";

  mbedtls_entropy_context entropy;
  mbedtls_ssl_config conf;
  mbedtls_x509_crt cacert;
  mbedtls_ctr_drbg_context ctr_drbg;

  mbedtls_ssl_config_init(&conf);
  mbedtls_x509_crt_init(&cacert);
  mbedtls_entropy_init(&entropy);
  mbedtls_ctr_drbg_init(&ctr_drbg);

  {
    int ret = mbedtls_ctr_drbg_seed(
          &ctr_drbg,
          mbedtls_entropy_func,
          &entropy,
          (const unsigned char *) pers,
          strlen(pers));
    if (ret < 0) throw std::invalid_argument("mbedtls_ctr_drbg_seed");
  }

  {
    int ret = mbedtls_x509_crt_parse(
          &cacert,
          reinterpret_cast<const unsigned char*>(mbedtls_test_cas_pem),
          mbedtls_test_cas_pem_len
    );
    if (ret < 0) throw std::invalid_argument("mbedtls_x509_crt_parse");
  }

  {
    int ret = mbedtls_ssl_config_defaults(
          &conf,
          MBEDTLS_SSL_IS_SERVER,
          MBEDTLS_SSL_TRANSPORT_STREAM,
          MBEDTLS_SSL_PRESET_DEFAULT
    );
    if (ret < 0) throw std::invalid_argument("mbedtls_ssl_config_defaults");
  }

  mbedtls_ssl_conf_rng(&conf, mbedtls_ctr_drbg_random, &ctr_drbg);
  mbedtls_ssl_conf_authmode(&conf, MBEDTLS_SSL_VERIFY_OPTIONAL);
  mbedtls_ssl_conf_ca_chain(&conf, &cacert, nullptr);

  f(conf);

  mbedtls_x509_crt_free(&cacert);
  mbedtls_ssl_config_free(&conf);
  mbedtls_entropy_free(&entropy);
  mbedtls_ctr_drbg_free(&ctr_drbg);
}

int main() {
}
